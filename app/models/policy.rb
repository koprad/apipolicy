class Policy < ActiveRecord::Base
 validates :number, uniqueness: {scope: :program}
end
