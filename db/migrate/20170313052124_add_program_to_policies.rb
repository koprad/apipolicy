class AddProgramToPolicies < ActiveRecord::Migration
  def change
    add_column :policies, :program, :string
  end
end
