class CreatePolicies < ActiveRecord::Migration
  def change
    create_table :policies do |t|
      t.datetime :insurance_started_at
      t.datetime :insurance_finished_at
      t.string :name

      t.timestamps null: false
    end
  end
end
